function varargout = UI(varargin)
% UI MATLAB code for UI.fig
%      UI, by itself, creates a new UI or raises the existing
%      singleton*.
%
%      H = UI returns the handle to a new UI or the handle to
%      the existing singleton*.
%
%      UI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UI.M with the given input arguments.
%
%      UI('Property','Value',...) creates a new UI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before UI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to UI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help UI

% Last Modified by GUIDE v2.5 07-Apr-2018 14:02:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @UI_OpeningFcn, ...
                   'gui_OutputFcn',  @UI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before UI is made visible.
function UI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to UI (see VARARGIN)

% Choose default command line output for UI
handles.output = hObject;
global barriers;
barriers = zeros(1);
global robots;
robots = zeros(1);
global axesHandle;
axesHandle = findobj('Tag', 'mapa');
global map;
map = zeros(1);
set(handles.popupmenu2, 'String', {'<HTML><FONT COLOR="red"><b>Cerven�</b></HTML>', '<HTML><FONT COLOR="blue"><b>Modr�</b></HTML>', '<HTML><FONT COLOR="green"><b>Zelen�</b></HTML>', '<HTML><FONT COLOR="yellow"><b><i>�lt�</i></b></HTML>', '<HTML><FONT COLOR="black"><b>Cierna</b></HTML>'})
% Update handles structure
guidata(hObject, handles);


% UIWAIT makes UI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = UI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double
if(isempty(get(hObject,'String')) == 0)
    input = str2double(get(hObject,'String'));
    if isnan(input)
        errordlg('Je nutn� zadat c�seln� hodnotu','Invalid Input','modal')
        uicontrol(hObject)
        return
    else
        global barriers;
        global robots;
        robotSize = size(robots);
        barrierSize = size(barriers);
        
        if(mod(input,1) ~= 0)
            input = floor(input);
        end 
        if (input < 10)
            input = 10;
        end
        if (input > 100)
            input = 100;
        end
        set(handles.edit3, 'String', input);
        removed = 0;
        if (barrierSize(1) == 5)
            for i = 1:barrierSize(2)
                i = i - removed;
                if(barriers(3, i) >= input)
                    barriers(:, i) = [];
                    barrierSize(2) = barrierSize(2) - 1;
                    if (barrierSize(1) == 0)
                        barriers = zero(1);
                        set(handles.edit5, 'String', '');
                        break;
                    end
                    removed = removed + 1;
                elseif ((barriers(2, i) + barriers(3, i))> input)
                    barriers(2, i) = input - barriers(3, i);
                end
            end
            if (removed > 0)
                mySize = size(barriers);
                set(handles.edit5, 'String', mySize(1));
            end
        end
        removed = 0;
        if (robotSize(1) == 2)
            for i = 1:robotSize(2)
                i = i - removed;
                if(robots(2, i) >= input)
                    robots(:, i) = [];
                    robotSize(2) = robotSize(2) - 1;
                    if (robotSize(1) == 0)
                        robots = zero(1);
                        set(handles.edit10, 'String', '');
                        break;
                    end
                    removed = removed + 1;
                end
            end
            if (removed > 0)
                mySize = size(robots);
                set(handles.edit10, 'String', mySize(1));
            end
        end
    end
end


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double
if(isempty(get(hObject,'String')) == 0)
    input = str2double(get(hObject,'String'));
    if isnan(input)
        errordlg('Je nutn� zadat c�seln� hodnotu','Invalid Input','modal')
        uicontrol(hObject)
        return
    else
        global barriers;
        global robots;
        robotSize = size(robots);
        barrierSize = size(barriers);
        
        if(mod(input,1) ~= 0)
            input = floor(input);
        end 
         if (input < 10)
            input = 10;
        end
        if (input > 100)
            input = 100;
        end
        set(handles.edit4, 'String', floor(input));
        removed = 0;
        if (barrierSize(1) == 5)
            for i = 1:barrierSize(2)
                i = i - removed;
                if(barriers(4, i) >= input)
                    barriers(:, i) = [];
                    barrierSize(2) = barrierSize(2) - 1;
                    if (barrierSize(1) == 0)
                        barriers = zero(1);
                        set(handles.edit5, 'String', '');
                        break;
                    end
                    removed = removed + 1;
                elseif ((barriers(1, i) + barriers(4, i)) > input)
                    barriers(1, i) = input - barriers(4, i);
                end
            end
            if (removed > 0)
                mySize = size(barriers);
                set(handles.edit5, 'String', mySize(1));
            end
        end
        removed = 0;
        if (robotSize(1) == 2)
            for i = 1:robotSize(2)
                i = i - removed;
                if(robots(1, i) >= input)
                    robots(:, i) = [];
                    robotSize(2) = robotSize(2) - 1;
                    if (robotSize(1) == 0)
                        robots = zero(1);
                        break;
                    end
                    removed = removed + 1;                
                    set(handles.edit10, 'String', '');
                    break;
                end
            end
            if (removed > 0)
                mySize = size(robots);
                set(handles.edit10, 'String', mySize(1));
            end
        end
    end
end


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
global barriers;
if(isempty(get(hObject,'String')) == 0)
    input = str2double(get(hObject,'String'));
    if isnan(input)
        errordlg('Je nutn� zadat c�seln� hodnotu','Invalid Input','modal')
        uicontrol(hObject)
        popUpMenuEntries = {'Zvol prek�ku'};
        set(handles.popupmenu1, 'String', popUpMenuEntries);
        return
    else
        if(mod(input,1) ~= 0)
            input = floor(input);
        end 
        if (input < 0)
            input = input * (-1);
        end
        if (input == 0)
            input = 1;
        end
        set(handles.edit5, 'String', floor(input));
        barrierSize = size(barriers);
        for i = 1:input
            popUpMenuEntries{i} = i;
        end
        mySize = size(popUpMenuEntries);
        if (barrierSize(1) == 1) 
            barriers = zeros(5, mySize(2));
        else
            if (barrierSize(2) > mySize(2))
                barriers = barriers(:, 1:mySize(2))
            elseif (barrierSize(2) < mySize(2))
                addColumns = zeros(5, mySize(2) - barrierSize(2)); 
                barriers = [barriers addColumns];
            end
        end
        set(handles.popupmenu1, 'String', popUpMenuEntries);
    end
end


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
global barriers;
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
contents = cellstr(get(hObject,'String'));
selected = contents{get(hObject,'Value')};
selected = str2num(selected);
if(barriers(1, selected) ~= 0)
    set(handles.edit6, 'String', barriers(1, selected));
    set(handles.edit7, 'String', barriers(2, selected));
    set(handles.edit8, 'String', barriers(3, selected));
    set(handles.edit9, 'String', barriers(4, selected));
    set(handles.popupmenu2, 'Value', barriers(5, selected));
else
    set(handles.edit6, 'String', '');
    set(handles.edit7, 'String', '');
    set(handles.edit8, 'String', '');
    set(handles.edit9, 'String', '');
    set(handles.popupmenu2, 'Value', 1);
end


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double
if (isempty(get(handles.edit4, 'String')))
        errordlg('Je nutn� najskor zadat vysku mapy');
        set(handles.edit6, 'String', '');
        return
end
if(isempty(get(hObject,'String')) == 0)
    input = str2double(get(hObject,'String'));
    if isnan(input)
        errordlg('Je nutn� zadat c�seln� hodnotu','Invalid Input','modal')
        uicontrol(hObject)
        return
    else
        if(mod(input,1) ~= 0)
            input = floor(input);
        end 
        if (input < 1)
            input = 1;
        end
        if (input > str2double(get(handles.edit4, 'String')))
            input = str2double(get(handles.edit4, 'String'));
        end
        set(handles.edit6, 'String', input);
    end
end


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double
if (isempty(get(handles.edit3, 'String')))
        errordlg('Je nutn� najskor zadat sirku mapy');
        set(handles.edit7, 'String', '');
        return
end
if(isempty(get(hObject,'String')) == 0)
    input = str2double(get(hObject,'String'));
    if isnan(input)
        errordlg('Je nutn� zadat c�seln� hodnotu','Invalid Input','modal')
        uicontrol(hObject)
        return
    else
        if(mod(input,1) ~= 0)
            input = floor(input);
        end 
        if (input < 1)
            input = 1;
        end
        if (input > str2double(get(handles.edit3, 'String')))
            input = str2double(get(handles.edit3, 'String'));
        end
        set(handles.edit7, 'String', input);
    end
end


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double
if (isempty(get(handles.edit6, 'String')))
        errordlg('Je nutn� najskor zadat sirku prekazky');
        set(handles.edit8, 'String', '');
        return
end
if(isempty(get(hObject,'String')) == 0)
    input = str2double(get(hObject,'String'));
    if isnan(input)
        errordlg('Je nutn� zadat c�seln� hodnotu','Invalid Input','modal')
        uicontrol(hObject)
        return
    else
        if(mod(input,1) ~= 0)
            input = floor(input);
        end 
        if (input < 0)
            input = 0;
        end
        if (str2double(get(handles.edit3, 'String')) < (input + str2double(get(handles.edit7, 'String'))))
            input = str2double(get(handles.edit3, 'String')) - str2double(get(handles.edit7, 'String'));
        end
        set(handles.edit8, 'String', floor(input));
    end
end


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double
if (isempty(get(handles.edit6, 'String')))
        errordlg('Je nutn� najskor zadat vysku prekazky');
        set(handles.edit8, 'String', '');
        return
end
if(isempty(get(hObject,'String')) == 0)
    input = str2double(get(hObject,'String'));
    if isnan(input)
        errordlg('Je nutn� zadat c�seln� hodnotu','Invalid Input','modal')
        uicontrol(hObject)
        return
    else
        if(mod(input,1) ~= 0)
            input = floor(input);
        end 
        if (input < 0)
            input = 0;
        end
        if (str2double(get(handles.edit4, 'String')) < (input + str2double(get(handles.edit6, 'String'))))
            input = str2double(get(handles.edit4, 'String')) - str2double(get(handles.edit6, 'String'));
        end
        set(handles.edit9, 'String', floor(input));
    end
end


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double
global robots;
if(isempty(get(hObject,'String')) == 0)
    input = str2double(get(hObject,'String'));
    if isnan(input)
        errordlg('Je nutn� zadat c�seln� hodnotu','Invalid Input','modal')
        uicontrol(hObject);
        popUpMenuEntries = {'Zvol robot'};
        set(handles.popupmenu3, 'String', popUpMenuEntries);
        return
    else
        if(mod(input,1) ~= 0)
            input = floor(input);
        end 
        if (input < 1)
            input = 1;
        end
        set(handles.edit10, 'String', floor(input));
        robotSize = size(robots);
        for i = 1:input
            popUpMenuEntries{i} = i;
        end
        mySize = size(popUpMenuEntries);
        if (robotSize(1) == 1) 
            robots = ones(2, mySize(2)) * (-1);
        else
            if (robotSize(2) > mySize(2))
                robots = robots(:, 1:mySize(2))
            elseif (robotSize(2) < mySize(2))
                addColumns = zeros(2, mySize(2) - robotSize(2)); 
                robots = [robots addColumns];
            end
        end
        set(handles.popupmenu3, 'String', popUpMenuEntries);
    end
end


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double
if(isempty(get(hObject,'String')) == 0)
    input = str2double(get(hObject,'String'));
    if isnan(input)
        errordlg('Je nutn� zadat c�seln� hodnotu','Invalid Input','modal')
        uicontrol(hObject)
        return
    else
        if(mod(input,1) ~= 0)
            input = floor(input);
        end 
        if (input < 0)
            input = input * (-1);
        end
        set(handles.edit11, 'String', floor(input));
    end
end


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double
if(isempty(get(hObject,'String')) == 0)
    input = str2double(get(hObject,'String'));
    if isnan(input)
        errordlg('Je nutn� zadat c�seln� hodnotu','Invalid Input','modal')
        uicontrol(hObject)
        return
    else
        if(mod(input,1) ~= 0)
            input = floor(input);
        end 
        if (input < 0)
            input = input * (-1);
        end
        set(handles.edit12, 'String', floor(input));
    end
end


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3
global robots;
contents = cellstr(get(hObject,'String'));
selected = contents{get(hObject,'Value')};
selected = str2num(selected);
robotSize = size(robots);
isSame = 0;
if(robots(1, selected) ~= -1)
     set(handles.edit11, 'String', barriers(1, selected));
     set(handles.edit12, 'String', barriers(2, selected));
end



% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global robots;
global barriers;
robots = [0;0];

mapWidth = randi([10 100], 1);
mapHeight = randi([10 100], 1);
set(handles.edit3, 'String', mapWidth);
set(handles.edit4, 'String', mapHeight);

barrierCount = randi([1 10], 1);
if (barrierCount < 1)
    barrierCount = 1;
end
widths = randi([1 mapWidth-5], 1, barrierCount);
heights = randi([1 mapHeight-5], 1, barrierCount);
xCorner = [];
yCorner = [];
widths
heights
for i = 1:barrierCount
    xCorner = [xCorner randi([1 mapWidth-widths(i)],1)];
    yCorner = [yCorner randi([1 mapHeight-heights(i)],1)];
end
colors = randi([1 5], 1, barrierCount);
barriers = [heights; widths; xCorner; yCorner; colors]
set(handles.edit5, 'String', barrierCount);
for i = 1:barrierCount
    popUpMenuEntries{i} = i;
end      
set(handles.popupmenu1, 'String', popUpMenuEntries);
set(handles.edit6, 'String', barriers(1,1));
set(handles.edit7, 'String', barriers(2,1));
set(handles.edit8, 'String', barriers(3,1));
set(handles.edit9, 'String', barriers(4,1));
set(handles.popupmenu2, 'Value', barriers(5, 1));
set(handles.edit10, 'String', '1');
set(handles.edit11, 'String', robots(1,1));
set(handles.edit12, 'String', robots(2,1));
set(handles.popupmenu3, 'String', {1});

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global barriers;
global robots;
robotContents = get(handles.popupmenu3,'String');
barrierContents = get(handles.popupmenu1,'String');
colorContents = get(handles.popupmenu2,'String');

robots(1, str2double(robotContents{get(handles.popupmenu3, 'Value')})) = str2double(get(handles.edit11, 'String'));
robots(2, str2double(robotContents{get(handles.popupmenu3, 'Value')})) = str2double(get(handles.edit12, 'String'));

colorIndex = find(contains(colorContents,colorContents{get(handles.popupmenu2, 'Value')}));
barriers(1, str2double(barrierContents{get(handles.popupmenu1, 'Value')})) = str2double(get(handles.edit6, 'String'));
barriers(2, str2double(barrierContents{get(handles.popupmenu1, 'Value')})) = str2double(get(handles.edit7, 'String'));
barriers(3, str2double(barrierContents{get(handles.popupmenu1, 'Value')})) = str2double(get(handles.edit8, 'String'));
barriers(4, str2double(barrierContents{get(handles.popupmenu1, 'Value')})) = str2double(get(handles.edit9, 'String'));
barriers(5, str2double(barrierContents{get(handles.popupmenu1, 'Value')})) = colorIndex;
barriers
robots


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global robots;
global barriers;
global axesHandle;
global map;

cla(axesHandle, 'reset');

mapHeight = str2num(char(get(handles.edit4,'String')));
if (isempty(mapHeight))
     errordlg('Pozor, nie je zadana Vyska mapy!');
     return
end

mapWidth = str2num(char(get(handles.edit3,'String')));
if (isempty(mapWidth))
     errordlg('Pozor, nie je zadana Sirka mapy!');
     return
end

map = zeros(mapHeight + 1, mapWidth + 1);
robotSize = size(robots);
for i = 1:robotSize(2)
    if (robots(1,i) == -1)
        errordlg(strcat('Pozor, nie su zadane parametre robota c.', num2str(i)));
        return
    end
    if (robots(1,i) > mapHeight || robots(2,i) > mapWidth)
        errordlg(strcat('Pozor, z mapy vytrca robot c.', num2str(i)));
        return
    end
end

barrierSize = size(barriers);
for i = 1:barrierSize(2)
    if (barriers(1,i) == 0 || isnan(barriers(1,i)))
        errordlg(strcat('Pozor, nie su zadane parametre prekazky c.', num2str(i)));
        return
    end
    for j = 1:robotSize(2)
        if (robots(1,j) >= barriers(3,i) & robots(1,j) <= (barriers(3,i) + barriers(2,i)) & robots(2,j) >= barriers(4,i) & robots(2,j) <= (barriers(4,i) + barriers(1,i)))
            warn1 = strcat('Pozor, robot c.', num2str(j));
            warn2 = strcat(' koliduje s prekazkou c.', num2str(i));
            errordlg(strcat(warn1, warn2));
            return
        end
    end
    if (barriers(1,i) + barriers(4,i) > mapHeight || barriers(2,i) + barriers(3,i) > mapWidth)
        errordlg(strcat('Pozor, z mapy pretrca prekazka c.', num2str(i)));
        return
    end
end

mapSize = size(map);
for i = 1:robotSize(2)
    x = robots(1, i);
    y = robots(2, i);
    plot(axesHandle,[x],[y], 'bo', 'Linewidth',3);
    map(mapSize(1) - y, x + 1) = 2;
    hold on;
end

for i = 1:barrierSize(2)
    x = barriers(3, i);
    y = barriers(4, i);
    bWidth = barriers(2, i);
    bHeight = barriers(1, i);
    color = getColor(barriers(5, i));
    for j = 0:bWidth
        for k = 0:bHeight
            map(mapSize(1) - y - k, x + 1 + j) = -1;
        end
    end
    map(mapSize(1) - y, x + 1) = 1
    plot(axesHandle, [x x+bWidth x+bWidth x x], [y y y+bHeight y+bHeight y], color, 'Linewidth',3);
end
% plot(axesHandle,[1 2 3],[1 2 3])

axis([0 mapWidth 0 mapHeight]);
grid minor
grid on

function [color] = getColor(number)
if (number == 1)
    color = 'red';
elseif (number == 2)
    color = 'blue';
elseif (number == 3)
    color = 'green';
elseif (number == 4)
    color = 'yellow';
else
    color = 'black';
end

    
